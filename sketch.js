var canvasWidth = 700, canvasHeight = 900;
var dots, goal;
var iterations = 1;

function setup() {
	createCanvas(canvasWidth, canvasHeight);
	frameRate(200);
	goal = createVector(canvasWidth / 2, 10);
	dots = new Population(1000);
}

function keyPressed() {
	if (key == '»') {
		iterations = 100;
	} else if (key == '½') {
		iterations = 1;
	}
}

function mousePressed() {
	if (mouseX > canvasWidth - 80 && mouseX < canvasWidth - 10 && mouseY > 10 && mouseY < 40) {
		dots.showParent = !dots.showParent;
	}
}

function draw() {
	background(0);
	fill(127, 255, 127);
	stroke(255);
	ellipse(goal.x, goal.y, 10);
	fill(0, 0, 255);
	rect(0, 300, 600, 20);
	rect(canvasWidth, 600, -600, 20);
	fill(255);
	stroke(0);
	textSize(24);
	text("Generation: " + dots.gen + "", 20, 20);
	fill(127);
	rect(canvasWidth - 80, 10, 70, 40);
	textSize(12);
	fill(0);
	text("show parent", canvasWidth - 78, 35);
	if (dots.allDotsDead()) {
		dots.calculateFitness();
		dots.naturalSelection();
		dots.mutation();
	} else {
		for (let i = 0; i < iterations; i++) {
			dots.update();
		}
		dots.show();
	}
}