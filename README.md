# A.I Dots

This is a Javascript clone of CodeBullet's A.I Dots game.

## Usage

Clone this Repository and open index.html or use a live-server or goto [A.I Dots](https://mjmardini.gitlab.io/a.i-dots).