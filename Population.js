class Population {
    constructor(size) {
        this.dots = new Array(size);
        for (let i = 0; i < this.dots.length; i++) {
            this.dots[i] = new Dot();
        }
        this.fitnessSum;
        this.gen = 1;
        this.bestDot = 0;
        this.minStep = 1000;
        this.showParent = false;
    }

    show() {
        for (let i = 0; i < this.dots.length; i++) {
            this.dots[i].showParent = this.showParent;
            this.dots[i].show();
        }
    }

    update() {
        for (let i = 0; i < this.dots.length; i++) {
            if (this.dots[i].brain.step > this.minStep) {
                this.dots[i].dead = true;
            } else {
                this.dots[i].update();
            }
        }
    }

    calculateFitness() {
        for (let i = 0; i < this.dots.length; i++) {
            this.dots[i].calculateFitness();
        }
    }

    allDotsDead() {
        for (let i = 0; i < this.dots.length; i++) {
            if (!this.dots[i].dead && !this.dots[i].reachedGoal) {
                return false;
            }
        }
        return true;
    }

    naturalSelection() {
        var newDots = new Array(this.dots.length);
        this.setBestDot();
        this.calculateFitnessSum();
        newDots[0] = this.dots[this.bestDot].getChild();
        newDots[0].isBest = true;
        for (let i = 1; i < newDots.length; i++) {
            let parent = this.selectParent();
            newDots[i] = parent.getChild();
        }
        this.dots = [].concat(newDots);
        this.gen++;
    }

    calculateFitnessSum() {
        this.fitnessSum = 0;
        for (let i = 0; i < this.dots.length; i++) {
            this.fitnessSum += this.dots[i].fitness;
        }
    }

    selectParent() {
        let rand = random(this.fitnessSum);
        let runningSum = 0;
        for (let i = 0; i < this.dots.length; i++) {
            runningSum += this.dots[i].fitness;
            if (runningSum > rand) {
                return this.dots[i];
            }
        }
    }

    mutation() {
        for (let i = 1; i < this.dots.length; i++) {
            this.dots[i].brain.mutate();
        }
    }

    setBestDot() {
        let max = 0;
        let maxIndex = 0;
        for (let i = 0; i < this.dots.length; i++) {
            if (this.dots[i].fitness > max) {
                max = this.dots[i].fitness;
                maxIndex = i;
            }
        }
        this.bestDot = maxIndex;
        if (this.dots[this.bestDot].reachedGoal) {
            this.minStep = this.dots[this.bestDot].brain.step;
        }
    }
}